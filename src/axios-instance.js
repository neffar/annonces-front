import axios from 'axios';

const instance = axios.create({
    //baseURL: 'https://interventions-975f3.firebaseio.com/'
    baseURL: 'http://api.project.com:3000'
});

export default instance;